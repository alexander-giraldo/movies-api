# Movies API

Technical assessment

## Heroku URL

```
https://movies-booking.herokuapp.com/
```

* GET        /api/:version/movies
* POST       /api/:version/movies
* POST       /api/:version/movies/:id/reservations
* GET        /api/:version/reservations

To run the app locally follow these steps:


## Getting Started

Software requirements

```
ruby 2.6.3
postgres
```

### Installing

Clone repo
```
git clone https://bitbucket.org/alexander-giraldo/movies-api.git
cd movies-api
```

Install gems

```
rvm gemset create movies-api && rvm gemset use movies-api
gem install bundler --no-document
bundle install
```

Create a database.yml inside the config directory

```
touch config/database.yml
```

Add connection parameters

```
#config/database.yml
defaults: &defaults
  adapter:  postgresql
  host:     localhost
  user: database_user
  password: database_password

development:
  <<: *defaults
  database: movies_api_dev

test:
  <<: *defaults
  database: movies_api_test
```

Create the database (development and test)
```
psql -U database_user postgres -c 'create database movies_api_dev;'
psql -U database_user postgres -c 'create database movies_api_test;'
```
## Tests

Run test suite

```
rake spec
```

## Running the API

Run the rack server

```
rackup
```

## Available Endpoints - Examples:

Usage examples

## Movies Endpoint

Display available endpoints

```
rake routes
```

### Create a new movie

```
curl -X POST "http://localhost:9292/api/v1/movies" -d "name=Star Wars&description=Movie description or synopsis&image_url=http://cdn.shopify.com/s/files/1/0061/3223/6357/products/SWANH1068L_first_grande.jpg?v=1566423330&days[]=1&days[]=2&days[]=3"

{"status":"ok","data":{"id":1,"name":"Star Wars","description":"Movie description or synopsis","image_url":"http://cdn.shopify.com/s/files/1/0061/3223/6357/products/SWANH1068L_first_grande.jpg?v=1566423330"}}
```

### List movies given a weekday. Days are represented by numbers (monday=1,tuesday=2,etc..)

```
curl "http://localhost:9292/api/v1/movies?day=2"

{"status":"ok","data":[{"id":1,"name":"Star Wars","description":"Movie description or synopsis","image_url":"http://cdn.shopify.com/s/files/1/0061/3223/6357/products/SWANH1068L_first_grande.jpg?v=1566423330"}]}
```

### Create Movie Reservation

There are multiple validations before creating a reservation.

* Reservation date is not in the past
* Movie is available on reservation day
* Reservation seats don't exceed the movie capacity, including previous reservations.

```
curl -X POST "http://localhost:9292/api/v1/movies/1/reservations" -d "full_name=John Doe&attend_date=2019-09-24&seats=4"

{"status":403,"data":"Date is not valid"}
```

```
curl -X POST "http://localhost:9292/api/v1/movies/1/reservations" -d "full_name=John Doe&attend_date=2019-09-30&seats=11"
{"status":403,"data":"The movie has not enough seats left on this date"}
```

```
curl -X POST "http://localhost:9292/api/v1/movies/1/reservations" -d "full_name=John Doe&attend_date=2019-10-21&seats=4"

{"status":201,"data":{"id":1,"movie_id":1,"attend_date":"2019-10-21","seats":4,"full_name":"John Doe"}}
```

### List reservations by date

```
curl "http://localhost:9292/api/v1/reservations?start_date=2019-09-26&end_date=2019-10-30"

{"status":200,"data":[{"id":1,"movie_id":1,"attend_date":"2019-10-21","seats":4,"full_name":"John Doe"}]}
```

## Authors

* **Alexander Giraldo** - (https://github.com/alexandergiraldo)

## License

This project is licensed under the MIT License
