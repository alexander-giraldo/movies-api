# frozen_string_literal: true

module API
  module V1
    class Base < Grape::API
      prefix "api"
      version "v1", using: :path
      format :json

      mount API::V1::Endpoints::Movies
      mount API::V1::Endpoints::Reservations
    end
  end
end
