# frozen_string_literal: true

module API
  module V1
    module Endpoints
      class Movies < Grape::API
        resource :movies do
          desc "List Movies by day"
          params do
            requires :day, type: String
          end
          get "/" do
            movies = Movie.join(:movie_days, movie_id: :id).
              select_all(:movies).
              where(Sequel.lit("movie_days.day = ?", params[:day].to_s)).
              map(&:to_json)

            { status: :ok, data: movies }
          end

          desc "Create Movie"
          params do
            requires :name, type: String
            requires :description, type: String
            requires :image_url, type: String
            requires :days, type: Array
          end
          post "/" do
            service = CreateMovie.new.call(params)
            status, entity = service.success? ? [201, service.value![:movie].to_json] : [400, ""]

            status status
            { status: status, data: entity }
          end

          desc "Create Movie Reservation"
          params do
            requires :full_name, type: String
            requires :id, type: Integer
            requires :attend_date, type: Date
            requires :seats, type: Integer
          end
          post "/:id/reservations" do
            service = CreateMovieReservation.new.call(params)

            if service.success?
              status, entity = [201, service.value![:reservation].to_json]
            else
              status, entity = [400, service.failure]
            end

            status status
            { status: status, data: entity }
          end
        end
      end
    end
  end
end
