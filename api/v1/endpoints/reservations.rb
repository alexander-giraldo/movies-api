# frozen_string_literal: true

module API
  module V1
    module Endpoints
      class Reservations < Grape::API
        resource :reservations do
          desc "List reservations between dates"
          params do
            requires :start_date, type: Date
            requires :end_date, type: Date
          end
          get "/" do
            reservations = Reservation.where((Sequel[:attend_date] >= params[:start_date]) & (Sequel[:attend_date] <= params[:end_date])).
              map(&:to_json)

            { status: :ok, data: reservations }
          end
        end
      end
    end
  end
end
