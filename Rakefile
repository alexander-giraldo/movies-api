# frozen_string_literal: true

require 'rubygems'
require 'bundler/setup'
require 'rake'

# rspec tasks
if Gem.loaded_specs.has_key?('rspec')
  require 'rspec/core'
  require 'rspec/core/rake_task'
  RSpec::Core::RakeTask.new(:spec)
end

task :environment do
  require File.expand_path('config/environment', __dir__)
  ENV['RACK_ENV'] ||= 'development'
end

task routes: :environment do
  API::Base.routes.each do |route|
    method = route.request_method.ljust(10)
    path = route.origin
    puts "     #{method} #{path}"
  end
end

namespace :db do
  desc "Migrate the database"
  task :migrate, [:version] => :environment do |_, args|
    version = args[:version] ? Integer(args[:version]) : nil
    Sequel.extension :migration
    Sequel::Migrator.apply(DB, File.join(File.dirname(__FILE__), 'db', 'migrations'), version)
  end

  desc "Generate a new migration file `new_migration[create_movies]`"
  task :generate, [:name] => :environment do |_, args|
    name = args[:name]
    abort("Missing migration file name") if name.nil?

    content = <<~STR
      # frozen_string_literal: true
      Sequel.migration do
        change do
        end
      end
    STR

    timestamp = Time.now.to_i
    filename = File.join(File.dirname(__FILE__), 'db', 'migrations', "#{timestamp}_#{name}.rb")
    File.write(filename, content)
    puts "Generated: #{filename}"
  end
end

task default: %i[spec]


