# frozen_string_literal: true

require "spec_helper"

describe API::Base do
  context "get to /api/v1/movies" do
    it "returns error when day parameter is not sent" do
      get "/api/v1/movies"
      expect(last_response.body).to eq '{"error":"day is missing"}'
    end

    it "returns a list of movies according to day parameter" do
      movie_day = FactoryBot.create(:movie_day)
      get "/api/v1/movies", { day: movie_day.day }

      expect(last_response.status).to eq 200
      expect(JSON.parse(last_response.body)["data"].map { |m| m["id"] }).to include movie_day.movie_id
    end
  end

  context "post to /api/v1/movies" do
    it "returns a movie entity" do
      post "/api/v1/movies", { name: "Back to future", description: "description....", image_url: "http://image.com/image.jpg", days: [1, 2] }

      expect(last_response.status).to eq 201
      expect(last_response.body).to include "Back to future"
    end
  end

  context "post to /api/v1/movies/:id/reservations" do
    it "returns a reservation entity" do
      movie_day = FactoryBot.create(:movie_day)
      post "/api/v1/movies/#{movie_day.movie_id}/reservations",
        {
          full_name: "John Doe",
          attend_date: "2019-10-21",
          seats: 4
        }

      expect(last_response.status).to eq 201
      expect(last_response.body).to include "John Doe"
    end

    it "returns an error due to invalid date" do
      post "/api/v1/movies/1/reservations",
        {
          full_name: "John Doe",
          attend_date: "2019-8-21",
          seats: 4
        }

      expect(last_response.status).to eq 400
      expect(last_response.body).to include "Date is not valid"
    end

    it "returns an error due to movie is not available in that date" do
      post "/api/v1/movies/1/reservations",
        {
          full_name: "John Doe",
          attend_date: "2019-10-26",
          seats: 4
        }

      expect(last_response.status).to eq 400
      expect(last_response.body).to include "The movie is not available on this date"
    end

    it "returns an error due to reservation exceeds capacity limit" do
      movie_day = FactoryBot.create(:movie_day)

      post "/api/v1/movies/#{movie_day.movie_id}/reservations",
        {
          full_name: "John Doe",
          attend_date: "2019-10-21",
          seats: 11
        }

      expect(last_response.status).to eq 400
      expect(last_response.body).to include "The movie has not enough seats left on this date"
    end
  end

  context "get to /api/v1/reservations" do
    it "returns a list of empty movies" do
      get "/api/v1/reservations", { start_date: Date.today, end_date: Date.today + 1.day }

      expect(last_response.status).to eq 200
      expect(JSON.parse(last_response.body)["data"]).to eq []
    end

    it "returns a list of movies between a time frame" do
      reservation = FactoryBot.create(:reservation)
      get "/api/v1/reservations", { start_date: Date.today, end_date: reservation.attend_date + 1.day }

      expect(last_response.status).to eq 200
      expect(JSON.parse(last_response.body)["data"].map { |m| m["id"] }).to eq [reservation.id]
    end
  end
end
