# frozen_string_literal: true

FactoryBot.define do
  factory :reservation do
    to_create &:save
    association :movie, factory: :movie, name: "Star Wars"
    full_name { "Jhon Doe" }
    attend_date { Date.today + 3.day}
    seats { 4 }
  end
end
