# frozen_string_literal: true

FactoryBot.define do

  factory :movie do
    to_create &:save
    name { "Star wars" }
    description { "Description..." }
    image_url { "http://www.image.com/image.jpg" }
  end
end
