# frozen_string_literal: true

FactoryBot.define do
  factory :movie_day do
    to_create &:save
    association :movie, factory: :movie, name: "Star Wars"
    day { 1 }
    total_capacity { 10 }
  end
end
