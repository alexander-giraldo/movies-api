# frozen_string_literal: true

$LOAD_PATH.unshift(File.join(File.dirname(__FILE__), "..", "api"))
$LOAD_PATH.unshift(File.dirname(__FILE__))

require "boot"

Dir[File.join(File.dirname(__FILE__), "..", "api", "v1", "endpoints", "*.rb")].each { |file| require file }
Dir[File.join(File.dirname(__FILE__), "..", "api", "v1", "*.rb")].each { |file| require file }
Dir[File.join(File.dirname(__FILE__), "..", "app", "models", "model_utils.rb")].each { |file| require file }

Dir[File.expand_path("../api/**/*.rb", __dir__)].each do |f|
  require f
end

Dir[File.expand_path("../app/**/*.rb", __dir__)].each do |f|
  require f
end

require "base"
