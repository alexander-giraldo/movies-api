# frozen_string_literal: true

require "rubygems"
require "bundler/setup"
require "yaml"

Bundler.require :default, (ENV["RACK_ENV"] || "development")

db_config_file = File.join(File.dirname(__FILE__), "database.yml")

if ENV["DATABASE_URL"] && ENV["RACK_ENV"] == "production"
  DB = Sequel.connect(ENV["DATABASE_URL"])
elsif File.exist?(db_config_file)
  config = YAML.load(File.read(db_config_file))
  DB = Sequel.connect(config[ENV["RACK_ENV"]])
end

Sequel.extension :migration
Sequel::Migrator.run(DB, File.join(File.dirname(__FILE__), "..", "db", "migrations")) if DB
