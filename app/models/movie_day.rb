# frozen_string_literal: true

class MovieDay < Sequel::Model(DB)
  include ::ModelUtils
  DEFAULT_CAPACITY = 10

  many_to_one :movie
end
