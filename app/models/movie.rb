# frozen_string_literal: true

class Movie < Sequel::Model(DB)
  include ::ModelUtils

  one_to_many :movie_days
end
