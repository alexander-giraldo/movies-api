# frozen_string_literal: true

class Reservation < Sequel::Model(DB)
  include ::ModelUtils

  many_to_one :movie
end
