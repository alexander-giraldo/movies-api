# frozen_string_literal: true

module ModelUtils
  def before_create
    self.created_at ||= Time.now
    self.updated_at ||= self.created_at
    super
  end

  def before_update
    self.updated_at = Time.now
    super
  end

  def to_json(except: [])
    to_hash.dup.delete_if { |key, _| except.include? key }
  end
end
