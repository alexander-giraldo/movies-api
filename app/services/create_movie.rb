# frozen_string_literal: true

require "dry/transaction"

class CreateMovie
  include Dry::Transaction

  step :create_movie
  step :create_movie_days

  private

  def create_movie(data)
    movie = Movie.create(name: data[:name], description: data[:description], image_url: data[:image_url])
    if movie
      Success(movie: movie, data: data)
    else
      Failure("movie was not created")
    end
  end

  def create_movie_days(movie:, data:)
    error = false
    days = data[:days] || []
    days.each do |day|
      error = true unless MovieDay.create(movie_id: movie.id, day: day, total_capacity: MovieDay::DEFAULT_CAPACITY)
    end

    if error
      Failure("movie was not created")
    else
      Success(movie: movie)
    end
  end
end
