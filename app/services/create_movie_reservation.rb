# frozen_string_literal: true

require "dry/transaction"

class CreateMovieReservation
  include Dry::Transaction

  step :validate_date
  step :validate_day
  step :validate_capacity
  step :create_reservation

  private

  def validate_date(data)
    if data[:attend_date] >= Date.today
      Success(data: data)
    else
      Failure("Date is not valid")
    end
  end

  def validate_day(data:)
    day = data[:attend_date].wday
    movie_day = MovieDay.where(movie_id: data[:id], day: day).last
    if movie_day
      Success(data: data, movie_day: movie_day)
    else
      Failure("The movie is not available on this date")
    end
  end

  def validate_capacity(data:, movie_day:)
    num_of_reservations = Reservation.where(attend_date: data[:attend_date]).sum(:seats) || 0
    total = num_of_reservations + data[:seats]

    if movie_day.total_capacity >= total
      Success(data: data)
    else
      Failure("The movie has not enough seats left on this date")
    end
  end

  def create_reservation(data:)
    reservation = Reservation.create(full_name: data[:full_name], movie_id: data[:id], attend_date: data[:attend_date], seats: data[:seats])
    if reservation
      Success(reservation: reservation, data: data)
    else
      Failure(reservation.errors)
    end
  end
end
