# frozen_string_literal: true

unless DB.table_exists?(:movie_days)
  Sequel.migration do
    change do
      create_table :movie_days do
        primary_key :id
        foreign_key :movie_id, :movies
        Integer :day
        Integer :total_capacity
        column :created_at, :timestamp
        column :updated_at, :timestamp
      end
    end
  end
end