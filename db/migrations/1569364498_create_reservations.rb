# frozen_string_literal: true
unless DB.table_exists?(:reservations)
  Sequel.migration do
    change do
      create_table :reservations do
        primary_key :id
        foreign_key :movie_id, :movies
        Date :attend_date
        Integer :seats
        String :full_name
        column :created_at, :timestamp
        column :updated_at, :timestamp
      end
    end
  end
end