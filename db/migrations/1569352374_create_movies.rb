# frozen_string_literal: true

unless DB.table_exists?(:movies)
  Sequel.migration do
    change do
      create_table :movies do
        primary_key :id
        String :name
        Text :description
        String :image_url
        column :created_at, :timestamp
        column :updated_at, :timestamp
      end
    end
  end
end